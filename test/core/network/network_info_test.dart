import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_clarika/core/network/network_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

void main() {
  NetworkInfoImpl networkInfoImpl;
  MockDataConnectionChecker mockDataConnectionChecker;

  setUp(() {
    mockDataConnectionChecker = MockDataConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockDataConnectionChecker);
  });

  test(
    'should call DataConnectionChecker.hasConnection when isConnected property is called',
    () async {
      // arrange
      final tIsConnected = Future.value(true);
      when(mockDataConnectionChecker.hasConnection).thenAnswer((_) => tIsConnected);
      // act
      final result = networkInfoImpl.isConnected;
      // assert
      verify(mockDataConnectionChecker.hasConnection);
      expect(result, tIsConnected);
    },
  );
}
