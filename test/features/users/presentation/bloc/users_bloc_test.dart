import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/domain/use_cases/get_all_users.dart';
import 'package:flutter_clarika/features/users/presentation/bloc/users_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockGetAllUsers extends Mock implements GetAllUsers {}

void main() {
  UsersBloc usersBloc;
  MockGetAllUsers mockGetAllUsers;

  setUp(() {
    mockGetAllUsers = MockGetAllUsers();
    usersBloc = UsersBloc(getAllUsers: mockGetAllUsers);
  });

  group('getAllUsers', () {
    final tUsers = [
      User(
        id: 1,
        firstName: 'Test',
        lastName: 'Test',
        location: 'Test',
        gender: 'Test',
        jobPosition: 'Test',
        photo: 'Test',
      ),
    ];

    void setUpMockGetAllUsersSuccess() {
      when(mockGetAllUsers()).thenAnswer((_) async => Right(tUsers));
    }

    void setUpMockGetAllUsersError(Failure failure) {
      when(mockGetAllUsers()).thenAnswer((_) async => Left(failure));
    }

    test(
      'should get users data',
      () async {
        // arrange
        setUpMockGetAllUsersSuccess();
        // act
        usersBloc.dispatch(GetAllUsersEvent());
        await untilCalled(mockGetAllUsers());
        // assert
        verify(mockGetAllUsers());
      },
    );

    test(
      'should emit Error state with proper mesage when getting data fails because of server failure',
          () async {
        // arrange
        setUpMockGetAllUsersError(ServerFailure());
        // assert
        final expected = [
          Empty(),
          Loading(),
          Error(SERVER_FAILURE_MESSAGE),
        ];
        expectLater(usersBloc.state, emitsInOrder(expected));
        // act
        usersBloc.dispatch(GetAllUsersEvent());
      },
    );

    test(
      'should emit Error state with proper message when getting data fails because of no internet connection on device',
          () async {
        // arrange
        setUpMockGetAllUsersError(NoConnectionFailure());
        // assert
        final expected = [
          Empty(),
          Loading(),
          Error(CONNECTION_FAILURE_MESSAGE),
        ];
        expectLater(usersBloc.state, emitsInOrder(expected));
        // act
        usersBloc.dispatch(GetAllUsersEvent());
      },
    );

    test(
      'should emit Loaded state after getting users data',
      () async {
        // arrange
        setUpMockGetAllUsersSuccess();
        // assert
        final expected = [
          Empty(),
          Loading(),
          Loaded(tUsers),
        ];
        expectLater(usersBloc.state, emitsInOrder(expected));
        // act
        usersBloc.dispatch(GetAllUsersEvent());
      },
    );
  });
}
