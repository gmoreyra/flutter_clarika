import 'dart:convert';

import 'package:flutter_clarika/core/errors/exceptions.dart';
import 'package:flutter_clarika/features/users/data/datasources/user_remote_data_source.dart';
import 'package:flutter_clarika/features/users/data/models/user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  UserRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = UserRemoteDataSourceImpl(client: mockHttpClient);
  });

  setUpMockHttpClientSuccess() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response(fixture('users.json'), 200));
  }

  setUpMockHttpClientError() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer((_) async => http.Response('Server error', 404));
  }

  group('getAllUsers', () {
    final tUsers = (jsonDecode(fixture('users.json')) as List).map((user) => UserModel.fromJson(user)).toList();

    test(
      'should make a GET http call through the provided client',
      () async {
        // arrange
        setUpMockHttpClientSuccess();
        // act
        dataSource.getAllUsers();
        // assert
        verify(mockHttpClient.get(
          'https://testapi.com/users',
          headers: {
            'Content-Type': 'application/json',
          },
        ));
      },
    );

    test(
      'should return List<User> when the status code is 200',
      () async {
        // arrange
        setUpMockHttpClientSuccess();
        // act
        final result = await dataSource.getAllUsers();
        // assert
        expect(result, equals(tUsers));
      },
    );

    test(
      'should throw a ServerException when the status code is not 200',
      () async {
        // arrange
        setUpMockHttpClientError();
        // act
        // assert
        expect(dataSource.getAllUsers, throwsA(isInstanceOf<ServerException>()));
      },
    );
  });
}
