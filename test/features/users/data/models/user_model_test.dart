import 'dart:convert';

import 'package:flutter_clarika/features/users/data/models/user_model.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tUserModel = UserModel(
      id: 1,
      firstName: 'Test',
      lastName: 'Test',
      location: 'Test',
      gender: 'Test',
      jobPosition: 'Test',
      photo: 'Test',
    );

  test('should be a subclass of User entity', () {
    expect(tUserModel, isA<User>());
  });
  
  group('fromJson', () {
    test('should return a valid UserModel', () {
      // arrange
      final Map<String, dynamic> json = jsonDecode(fixture('user.json'));
      // act
      final result = UserModel.fromJson(json);
      // assert
      expect(result, tUserModel);
    });
  });
}
