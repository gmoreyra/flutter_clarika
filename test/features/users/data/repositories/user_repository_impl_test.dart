import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/core/errors/exceptions.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/core/network/network_info.dart';
import 'package:flutter_clarika/features/users/data/datasources/user_remote_data_source.dart';
import 'package:flutter_clarika/features/users/data/repositories/user_repository_impl.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRemoteDataSource extends Mock implements UserRemoteDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  UserRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = UserRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  test('should check if device is online', () {
    //arrange
    when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
    // act
    repository.getAllUsers();
    // assert
    verify(mockNetworkInfo.isConnected);
  });

  group('device is online', () {
    final tUsers = [
      User(
        id: 1,
        firstName: 'Test',
        lastName: 'Test',
        location: 'Test',
        gender: 'Test',
        jobPosition: 'Test',
        photo: 'Test',
      ),
    ];

    setUp(() {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
    });

    test(
      'should return users data when the call to remote data source is successful',
      () async {
        // arrange
        when(mockRemoteDataSource.getAllUsers()).thenAnswer((_) async => tUsers);
        // act
        final result = await repository.getAllUsers();
        // assert
        verify(mockRemoteDataSource.getAllUsers());
        expect(result, equals(Right(tUsers)));
      },
    );

    test(
      'should return server failure when the call to remote data source is not successful',
      () async {
        // arrange
        when(mockRemoteDataSource.getAllUsers()).thenThrow(ServerException());
        // act
        final result = await repository.getAllUsers();
        // assert
        verify(mockRemoteDataSource.getAllUsers());
        expect(result, equals(Left(ServerFailure())));
      },
    );
  });

  group('device is offline', () {
    setUp(() {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
    });

    test(
      'should return no connection failure when calling to remote data source',
      () async {
        // arrange
        // act
        final result = await repository.getAllUsers();
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(Left(NoConnectionFailure())));
      },
    );
  });
}
