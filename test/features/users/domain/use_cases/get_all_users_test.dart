import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/domain/repositories/user_repository.dart';
import 'package:flutter_clarika/features/users/domain/use_cases/get_all_users.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockUserRepository extends Mock implements UserRepository {}

void main() {
  GetAllUsers usecase;
  MockUserRepository mockUserRepository;

  setUp(() {
    mockUserRepository = MockUserRepository();
    usecase = GetAllUsers(mockUserRepository);
  });

  final tUser = [
    User(
      id: 1,
      firstName: 'Test',
      lastName: 'Test',
      location: 'Test',
      gender: 'Test',
      jobPosition: 'Test',
      photo: 'Test',
    ),
  ];

  test(
    'should get all users data',
    () async {
      // arrange
      when(mockUserRepository.getAllUsers()).thenAnswer((_) async => Right(tUser));
      // act
      final result = await usecase();
      // assert
      expect(result, Right(tUser));
      verify(mockUserRepository.getAllUsers());
      verifyNoMoreInteractions(mockUserRepository);
    },
  );
}
