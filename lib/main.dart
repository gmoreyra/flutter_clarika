import 'package:flutter/material.dart';
import 'package:flutter_clarika/features/users/presentation/pages/users_page.dart';
import 'di_container.dart' as di_container;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di_container.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.pink,
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      routes: {
        '/': (context) => UsersPage(),
      },
    );
  }
}
