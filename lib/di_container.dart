import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_clarika/core/network/network_info.dart';
import 'package:flutter_clarika/features/users/data/datasources/user_remote_data_source.dart';
import 'package:flutter_clarika/features/users/data/repositories/user_repository_impl.dart';
import 'package:flutter_clarika/features/users/domain/repositories/user_repository.dart';
import 'package:flutter_clarika/features/users/domain/use_cases/get_all_users.dart';
import 'package:flutter_clarika/features/users/presentation/bloc/users_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

GetIt getIt = GetIt.instance;

Future<void> init() async {
  getIt.registerFactory(
    () => UsersBloc(
      getAllUsers: getIt(),
    ),
  );

  getIt.registerLazySingleton(() => GetAllUsers(getIt()));

  getIt.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      networkInfo: getIt(),
      remoteDataSource: getIt(),
    ),
  );

  getIt.registerLazySingleton<UserRemoteDataSource>(
    () => UserRemoteDataSourceImpl(
      client: getIt(),
    ),
  );

  getIt.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(getIt()));
  getIt.registerLazySingleton(() => http.Client());
  getIt.registerLazySingleton(() => DataConnectionChecker());
}
