import 'package:flutter/material.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/user_property_tile.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserDetailsPage extends StatelessWidget {
  final User user;

  const UserDetailsPage({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.completeName),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.35,
            child: Hero(
              tag: user.id,
              child: Image.network(
                user.photo,
                fit: BoxFit.cover,
              ),
              transitionOnUserGestures: true,
            ),
          ),
          UserPropertyTile(
            icon: FontAwesomeIcons.idCard,
            label: 'Nombre completo',
            content: user.completeName,
          ),
          UserPropertyTile(
            icon: FontAwesomeIcons.solidClipboard,
            label: 'Puesto de trabajo',
            content: user.jobPosition,
          ),
          UserPropertyTile(
            icon: FontAwesomeIcons.mapMarkedAlt,
            label: 'Ubicación',
            content: user.location,
          ),
          UserPropertyTile(
            icon: FontAwesomeIcons.venusMars,
            label: 'Género',
            content: user.gender,
          ),
        ],
      ),
    );
  }
}
