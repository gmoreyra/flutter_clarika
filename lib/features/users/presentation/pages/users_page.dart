import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clarika/di_container.dart';
import 'package:flutter_clarika/features/users/presentation/bloc/users_bloc.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/error_message.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/loading_indicator.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/users_list.dart';

class UsersPage extends StatefulWidget {
  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  UsersBloc _usersBloc = getIt<UsersBloc>();

  @override
  void initState() {
    super.initState();
    _usersBloc.dispatch(GetAllUsersEvent());
  }

  @override
  void dispose() {
    _usersBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Proyecto Clarika'),
      ),
      body: BlocProvider(
        builder: (_) => _usersBloc,
        child: BlocBuilder<UsersBloc, UsersState>(
          builder: (context, state) {
            if (state is Empty) {
              return Container();
            } else if (state is Loading) {
              return LoadingIndicator();
            } else if (state is Loaded) {
              return UsersList(
                users: state.users,
              );
            } else {
              return ErrorMessage(
                message: (state as Error).message,
              );
            }
          },
        ),
      ),
    );
  }
}
