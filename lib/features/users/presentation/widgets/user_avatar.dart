import 'package:flutter/material.dart';

class UserAvatar extends StatelessWidget {
  final String photoUrl;
  final double borderRadius;

  const UserAvatar({
    Key key,
    @required this.photoUrl,
    @required this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: borderRadius,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius),
        child: FadeInImage.assetNetwork(
          placeholder: 'assets/logo.png',
          width: borderRadius * 2,
          height: borderRadius * 2,
          image: photoUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
