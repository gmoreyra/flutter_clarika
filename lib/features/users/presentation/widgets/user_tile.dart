import 'package:flutter/material.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/presentation/pages/user_details_page.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/user_avatar.dart';

class UserTile extends StatelessWidget {
  final User user;

  const UserTile({
    Key key,
    @required this.user,
  })  : assert(user != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return UserDetailsPage(user: user);
          },
          fullscreenDialog: true,
        ),
      ),
      leading: Hero(
        tag: user.id,
        child: UserAvatar(
          borderRadius: 25.0,
          photoUrl: user.photo,
        ),
        transitionOnUserGestures: true,
      ),
      title: Text(user.completeName),
      subtitle: Text('Género: ${user.gender}'),
    );
  }
}
