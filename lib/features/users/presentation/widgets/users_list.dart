import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/presentation/bloc/users_bloc.dart';
import 'package:flutter_clarika/features/users/presentation/widgets/user_tile.dart';

class UsersList extends StatelessWidget {
  final List<User> users;

  const UsersList({Key key, this.users}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async => BlocProvider.of<UsersBloc>(context).dispatch(GetAllUsersEvent()),
      child: ListView(
        children: ListTile.divideTiles(
          context: context,
          color: Colors.blueGrey,
          tiles: users.map((user) => UserTile(user: user)).toList(),
        ).toList(),
      ),
    );
  }
}
