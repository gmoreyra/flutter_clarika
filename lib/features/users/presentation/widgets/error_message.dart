import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clarika/features/users/presentation/bloc/users_bloc.dart';

class ErrorMessage extends StatelessWidget {
  final String message;

  const ErrorMessage({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            message,
          ),
          RaisedButton(
            child: Text('REINTENTAR'),
            onPressed: () => BlocProvider.of<UsersBloc>(context).dispatch(GetAllUsersEvent()),
          ),
        ],
      ),
    );
  }
}
