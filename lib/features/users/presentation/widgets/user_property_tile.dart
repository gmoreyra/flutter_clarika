import 'package:flutter/material.dart';

class UserPropertyTile extends StatelessWidget {
  final String label;
  final String content;
  final IconData icon;

  const UserPropertyTile({
    Key key,
    @required this.label,
    @required this.content,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      title: Text(label),
      subtitle: Text(content),
    );
  }
}
