part of 'users_bloc.dart';

@immutable
abstract class UsersState extends Equatable {
  UsersState([List props = const <dynamic>[]]) : super(props);
}

class Empty extends UsersState {}

class Loading extends UsersState {}

class Loaded extends UsersState {
  final List<User> users;

  Loaded(this.users);
}

class Error extends UsersState {
  final String message;

  Error(this.message);
}