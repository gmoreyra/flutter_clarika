part of 'users_bloc.dart';

@immutable
abstract class UsersEvent extends Equatable {
  UsersEvent([List props = const <dynamic>[]]) : super(props);
}

class GetAllUsersEvent extends UsersEvent {}