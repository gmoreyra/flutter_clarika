import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/domain/use_cases/get_all_users.dart';
import 'package:meta/meta.dart';

part 'users_event.dart';

part 'users_state.dart';

const String SERVER_FAILURE_MESSAGE = 'Error de conexión al servidor.';
const String CONNECTION_FAILURE_MESSAGE = 'No tienes conexión a internet.';
const String UNKNOWN_FAILURE_MESSAGE = 'Error desconocido.';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final GetAllUsers getAllUsers;

  UsersBloc({
    @required this.getAllUsers,
  });

  @override
  UsersState get initialState => Empty();

  @override
  Stream<UsersState> mapEventToState(UsersEvent event) async* {
    if (event is GetAllUsersEvent) {
      yield Loading();
      final users = await getAllUsers();
      yield* users.fold(
        (failure) async* {
          switch (failure.runtimeType) {
            case ServerFailure:
              yield Error(SERVER_FAILURE_MESSAGE);
              break;
            case NoConnectionFailure:
              yield Error(CONNECTION_FAILURE_MESSAGE);
              break;
            default:
              yield Error(UNKNOWN_FAILURE_MESSAGE);
          }
        },
        (data) async* {
          yield Loaded(data);
        },
      );
    }
  }
}
