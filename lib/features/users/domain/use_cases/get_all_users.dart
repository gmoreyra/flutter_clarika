import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/domain/repositories/user_repository.dart';

class GetAllUsers {
  final UserRepository repository;

  GetAllUsers(this.repository);

  Future<Either<Failure, List<User>>> call() async {
    return repository.getAllUsers();
  }
}