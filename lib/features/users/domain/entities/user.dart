import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User extends Equatable {
  final int id;
  final String firstName;
  final String lastName;
  final String jobPosition;
  final String gender;
  final String location;
  final String photo;

  String get completeName => '$firstName $lastName';

  User({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.jobPosition,
    @required this.gender,
    @required this.location,
    @required this.photo,
  }) : super([id, firstName, lastName, jobPosition, gender, location, gender]);
}
