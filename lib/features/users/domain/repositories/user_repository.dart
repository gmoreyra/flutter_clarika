import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';

abstract class UserRepository {
  Future<Either<Failure, List<User>>> getAllUsers();
}