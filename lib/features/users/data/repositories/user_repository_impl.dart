import 'package:dartz/dartz.dart';
import 'package:flutter_clarika/core/errors/exceptions.dart';
import 'package:meta/meta.dart';
import 'package:flutter_clarika/core/errors/failures.dart';
import 'package:flutter_clarika/core/network/network_info.dart';
import 'package:flutter_clarika/features/users/data/datasources/user_remote_data_source.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:flutter_clarika/features/users/domain/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  UserRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<User>>> getAllUsers() async {
    if (await networkInfo.isConnected) {
      try {
        var usersData = await remoteDataSource.getAllUsers();
        return Right(usersData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
