import 'dart:convert';

import 'package:flutter_clarika/core/errors/exceptions.dart';
import 'package:flutter_clarika/features/users/data/models/user_model.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

const String API_URL = 'https://my-json-server.typicode.com/juanagu/mobile-interview';

abstract class UserRemoteDataSource {
  Future<List<User>> getAllUsers();
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final http.Client client;

  UserRemoteDataSourceImpl({
    @required this.client,
  });

  @override
  Future<List<User>> getAllUsers() async {
    final response = await client.get('$API_URL/users', headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return (jsonDecode(response.body) as List).map((user) => UserModel.fromJson(user)).toList();
    } else {
      throw ServerException();
    }
  }
}
