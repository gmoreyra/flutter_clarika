import 'package:flutter/cupertino.dart';
import 'package:flutter_clarika/features/users/domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    @required int id,
    @required String firstName,
    @required String lastName,
    @required String jobPosition,
    @required String gender,
    @required String location,
    @required String photo,
  }) : super(id: id, firstName: firstName, lastName: lastName, jobPosition: jobPosition, gender: gender, location: location, photo: photo);

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      jobPosition: json['job_position'],
      gender: json['gender'],
      location: json['location'],
      photo: json['photo'],
    );
  }
}
